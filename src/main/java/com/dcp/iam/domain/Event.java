package com.dcp.iam.domain;

import java.io.Serializable;

public record Event(String key, Integer ts, String event, String json) implements Serializable {
}
