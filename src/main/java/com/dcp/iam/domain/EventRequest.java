package com.dcp.iam.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class EventRequest {
    private String key;
    private Integer ts;
    private String event;
    private String json;
    private String companyName;

    public EventRequest withCompanyName(String companyName) {
        this.companyName = companyName;
        return this;
    }
}
