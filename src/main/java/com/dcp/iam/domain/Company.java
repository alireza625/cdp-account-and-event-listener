package com.dcp.iam.domain;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Getter
@Entity
@Table(name = "companies")
public class Company {
    @Id
    private String name;
    @Column(nullable = false)
    private String password;

    public AuthenticatedCompany authenticate(PasswordEncoder encoder, String rawPassword) {
        if (!encoder.matches(rawPassword, password))
            throw new RuntimeException("bad credentials");
        return new AuthenticatedCompany(name);
    }

}
