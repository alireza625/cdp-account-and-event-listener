package com.dcp.iam.app;

import com.dcp.iam.domain.Company;
import org.springframework.data.repository.CrudRepository;

public interface CompaniesRepository extends CrudRepository<Company, String> {
}
